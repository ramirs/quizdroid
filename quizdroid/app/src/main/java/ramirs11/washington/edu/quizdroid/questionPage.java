package ramirs11.washington.edu.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;


public class questionPage extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_page);

        final Intent i = getIntent();
        final questionMaster questions = (questionMaster) i.getSerializableExtra("questionMaster");
        final String topicName = i.getStringExtra("name");

        TextView textView = (TextView) findViewById(R.id.qTopic);
        textView.setText(topicName);
        //System.out.println(Arrays.toString(questions.questions())); //all questions
        final String question = questions.getQuestion();
        textView = (TextView) findViewById(R.id.qDesc);
        textView.setText(question);
        //System.out.println((questions.getQuestion())); //current question
        String[] options = questions.getIndex();

        final RadioGroup btnGroup = (RadioGroup) findViewById(R.id.optionGroup);
        RadioButton btn = (RadioButton) findViewById(R.id.option1);
        btn.setText(options[0]);
        btn = (RadioButton) findViewById(R.id.option2);
        btn.setText(options[1]);
        btn = (RadioButton) findViewById(R.id.option3);
        btn.setText(options[2]);
        btn = (RadioButton) findViewById(R.id.option4);
        btn.setText(options[3]);
        //System.out.println(Arrays.toString(questions.getIndex())); //options for question at index, index 0 is right ans
        //System.out.println(questions.questionCount());          //number of questions

        Log.i("questionPage.java: question", questions.getQuestion());
        Log.i("questionPage.java: options", Arrays.toString(questions.getIndex()));
        Log.i("questionPage.java: questionMaster index", ""+ questions.index);

        Button b = (Button)  findViewById(R.id.btnSubmit);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                1) Quiz println() working
                2) transition to results view AFTER selecting a RadioButton

                3) results highlight correct answer / wrong answer
                4) transition back to question (loop)
                if (questions.nextQuestion()) {
                        System.out.println(questions.getQuestion());
                        System.out.println(Arrays.toString(questions.getIndex()));
                    } else {
                        System.out.println("all done!... results?");
                    }
                5) when done, transition to final results?
                 */
                int selected = btnGroup.getCheckedRadioButtonId();
                if(selected == -1){ //no selection has been made yet
                    Toast.makeText(questionPage.this, "Please select an option", Toast.LENGTH_SHORT).show();
                } else {
                    //RadioButton btnSelected = (RadioButton) findViewById(selected);
                    //System.out.println("Selected: " + selected);
                    //System.out.println(btnSelected.getText());
                    Intent i = new Intent(questionPage.this, Results.class);
                    i.putExtra("selected", selected);
                    i.putExtra("name", topicName);
                    i.putExtra("question", question);
                    i.putExtra("questionMaster", questions);

                    startActivity(i);
                    finish();
                }
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_question_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

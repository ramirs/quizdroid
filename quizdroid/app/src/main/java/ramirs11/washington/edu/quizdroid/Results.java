package ramirs11.washington.edu.quizdroid;

import android.content.Intent;
import android.graphics.Color;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Arrays;


public class Results extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        Intent i = getIntent();

        final questionMaster questions = (questionMaster) i.getSerializableExtra("questionMaster");
        int check = i.getIntExtra("selected", 0);
        TextView textView = (TextView) findViewById(R.id.qDesc);
        textView.setText(i.getStringExtra("question"));
        final String name = i.getStringExtra("name");

        RadioGroup options = (RadioGroup) findViewById(R.id.optionGroup);
        String[] optionText = questions.getIndex();
        RadioButton btn = (RadioButton) findViewById(R.id.option1);
        btn.setText(optionText[0]);
        btn = (RadioButton) findViewById(R.id.option2);
        btn.setText(optionText[1]);
        btn = (RadioButton) findViewById(R.id.option3);
        btn.setText(optionText[2]);
        btn = (RadioButton) findViewById(R.id.option4);
        btn.setText(optionText[3]);

        btn = (RadioButton) findViewById(check);
        textView = (TextView) findViewById(R.id.answerResponse);
        if(check == findViewById(R.id.option1).getId()){ //could make this a call on questionMaster function to check for each dynamic question group
            textView.setText("Correct!!!");
            questions.correct();
        } else {
            textView.setText("Unacceptable!!!");
            btn.setBackgroundColor(Color.rgb(255, 144, 115));
        }
        TextView score = (TextView) findViewById(R.id.score);
        score.setText(questions.getScore());

        Button nextQ = (Button) findViewById(R.id.actionAns);
        nextQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (questions.nextQuestion()) {
                    Log.i("Results.java: question", questions.getQuestion());
                    Log.i("Results.java: options", Arrays.toString(questions.getIndex()));
                    Log.i("Results.java: questionMaste index", ""+ questions.index);

                    Intent i = new Intent(Results.this, questionPage.class);
                    i.putExtra("questionMaster", questions);
                    i.putExtra("name", name);

                    startActivity(i); //starts MainActivity
                    finish(); //kills MainActivity2
                } else {
                    System.out.println("all done!... results?");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_results, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

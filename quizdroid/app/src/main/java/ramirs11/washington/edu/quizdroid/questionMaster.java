package ramirs11.washington.edu.quizdroid;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by iguest on 2/3/15.
 */
public class questionMaster implements Serializable{

    public HashMap<String, String[]> map;
    public int score;
    public int index;

    public questionMaster(){
        map = new HashMap<>();
        score = 0;
        index = 0;
    }

    public int questionCount(){
        return map.size();
    }

    public void correct(){
        score++;
    }

    public String getScore(){
        return score + " / " + map.size();
    }

    /*returns the multiple choice strings for current question*/
    public String[] getIndex(){
        int count = 0;
        for(String s : map.keySet()){
          //System.out.println("count : " + count + ", index: " + index);
            if (count == index){
                //System.out.println((Arrays.toString(map.get(s))));
                return map.get(s);
          }
          count++;
        }
        return null;
    }

    /*on next button press, make sure this will go through each question*/
    public boolean nextQuestion(){
        if (index + 1 < map.size()){
            index++;
            return true;
        } else {
            return false;
        }
    }

    /*full set of questions*/
    public String[] questions(){
        return map.keySet().toArray(new String[0]);
    }

    /*question at index*/
    public String getQuestion(){
        String[] questions = map.keySet().toArray(new String[0]);
        return questions[index];
    }

    public String toString(){
        return map.toString();
    }
}

package ramirs11.washington.edu.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.reflect.Array;


public class topicOverview extends ActionBarActivity implements View.OnClickListener{

    public String name;
    public int num;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_overview);

        Intent i = getIntent();
        name = i.getStringExtra("name");
        String desc = i.getStringExtra("desc");
        num = i.getIntExtra("num", 0);

        TextView v = (TextView) findViewById(R.id.qTopic);
        v.setText(name);
        v = (TextView) findViewById(R.id.tDesc);
        v.setText(desc);
        v = (TextView) findViewById(R.id.questionNum);
        v.setText(num + " questions total");

        Button b = (Button) findViewById(R.id.startQuiz);
        b.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_topic_overview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v){
        Intent i = new Intent(topicOverview.this, questionPage.class);
        questionMaster questions = new questionMaster();
        switch (name){
            case "Math":
                questions.map.put("28 + 16 = ???", new String[]{"44", "23", "72", "47"});
                questions.map.put("16 x 9 = ???", new String[]{"144", "57", "22", "164"});
                questions.map.put("32 / 40 = ???", new String[]{"1.25", "0.8", "1.5", "0.85"});
                questions.map.put("3 x 29 = ???", new String[]{"87", "90", "82", "76"});
                i.putExtra("questionMaster", questions);
                break;
            case "Physics":
                questions.map.put("Which isn’t a cardinal direction?", new String[]{"M", "N", "S", "E"});
                questions.map.put("Unit of gravity on earth?", new String[]{"9.807 m/s^2", "2.3 m/s^2", "9.807 m/s", "42"});
                questions.map.put("How many stars are in the milky way galaxy?", new String[]{"~300 billion", "~2.2 billion", "~600 million", "1"});
                i.putExtra("questionMaster", questions);
                break;
            case "Marvel Superheroes":
                questions.map.put("Which Avenger is named Clint Barton and honed their skills training in a circus?",
                        new String[]{"Hawkeye", "Iron Man", "Thor", "Hulk"});
                questions.map.put("What does S.H.I.E.L.D. stand for?",
                        new String[]{"Strategic Homeland Intervention, Enforcement and Logistics Division",
                                "Something Horrible If Ever Left Divided", "Captain America's Shield",
                                "Strategic Hierarchical Information Efficiency Lexicon Dubstep"});
                questions.map.put("In Norse mythology, Loki is the god of what?",
                        new String[]{"Mischief", "Luck", "Death", "Pandas"});
                questions.map.put("What is the name of Thor’s hammer?",
                        new String[]{"Mjolnir", "The Rock", "Deathstick", "Phil"});
                questions.map.put("In the original Marvel comics, who gifted Captain America’s trademark shield to him?",
                        new String[]{"President Franklin Roosevelt", "Dr. Martin Luther King", "Michael Jackson", "Michael Eisenberg"});
                i.putExtra("questionMaster", questions);
                break;
            case "Seahawks Players":
                questions.map.put("Who was the first Seahawk inducted into the Pro Football Hall of Fame?",
                        new String[]{"Steve Largent", "Billy Bob Thornton", "Russell Wilson", "Ken Griffey Jr."});
                questions.map.put("Which quarterback is the Seahawks’ all-time leader in career passing yards?",
                        new String[]{"Matt Hasselbeck", "Russell Wilson", "Tavaris Jackson", "Shaun Alexander"});
                questions.map.put("What is the name of the Seahawks’ hawk?",
                        new String[]{"Taima", "Rain", "Beastmode", "Phil"});
                questions.map.put("Which jersey number(s) is retired by the Seahawks?",
                        new String[]{"12 & 80", "12", "69 & 12", "00"});
                i.putExtra("questionMaster", questions);
                break;
            default:
                break;
        }
        i.putExtra("name", name);
        i.putExtra("num", num); //redundant, can get this from questionMaster.questionCount()
        startActivity(i);
        finish();
    }

    public String[] getArray(String name){
        if (name.equals("Math")){
            return new String[]{"1", "2", "3", "4"};
        } else {
            return new String[]{"oops..."};
        }
    }

}

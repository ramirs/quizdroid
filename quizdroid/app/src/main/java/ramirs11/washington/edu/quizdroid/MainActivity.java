package ramirs11.washington.edu.quizdroid;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button) findViewById(R.id.mathBtn);
        b.setOnClickListener(this);
        b = (Button) findViewById(R.id.physicsBtn);
        b.setOnClickListener(this);
        b = (Button) findViewById(R.id.heroBtn);
        b.setOnClickListener(this);
        b = (Button) findViewById(R.id.hawkBtn);
        b.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClick(View v){
        Intent i = new Intent(MainActivity.this, topicOverview.class);
        switch (v.getId()){
            case R.id.mathBtn:
                System.out.println("maths");
                i.putExtra("name", "Math");
                i.putExtra("desc", "Math is fun. Math is cool. Let's do some Math.");
                i.putExtra("num", 4);
                break;
            case R.id.physicsBtn:
                System.out.println("phy");
                i.putExtra("name", "Physics");
                i.putExtra("desc", "Physics is fun. Physics is cool. Let's do some Physics.");
                i.putExtra("num", 3);
                break;
            case R.id.heroBtn:
                System.out.println("heroo");
                i.putExtra("name", "Marvel Superheroes");
                i.putExtra("desc", "The Avengers is the greatest superhero franchise of all time " +
                        "and you know it. Let's put your love of Joss Whedon to the test.");
                i.putExtra("num", 5);
                break;
            case R.id.hawkBtn:
                System.out.println("caCAW");
                i.putExtra("name", "Seahawks Players");
                i.putExtra("desc", "SEA.... HAWKS..... SEA..... HAWKS.");
                i.putExtra("num", 4);
                break;
            default:
                break;
        }
        startActivity(i);
        finish();
    }
}
